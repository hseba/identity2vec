# Identity2Vec

Learning Mesoscopic Structural Identity Representations via Poisson Probability Metric

## Installation

The codes are written in Python 3 environment.

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the necessary python libraries

```bash
pip install gensim
pip install numpy
pip install networkx
pip install tqdm
```

## Abstract

Structural properties in a network have mostly been defined in terms of their microscopic level and macroscopic level. Many existing studies focus mainly on learning representations for vertices in similar local proximity such as the first-order proximity (microscopic level), while some other studies describe the global network connectivity and characteristics (macroscopic level). It is however important to understand the substructures between the vertex and network levels so as to capture the structural identities exhibited by vertices. In this paper, we present a novel and flexible approach for learning representations for structural identities of vertices in a network, such that set of vertices with similar structural identity are mapped closely in the low latent embedding space. Our approach applies the Poisson distribution-based measure and a divergence estimation score to capture nodes' structural similarities at k-hop proximity via a guided walk; and using the Skipgram model, we learn embeddings for nodes with similar structural identity captured in the walk. Experiments with real-life datasets validate our approach, as well as gave superior performance when compared with existing models on link prediction, node classification, and learning-to-rank tasks.
```

